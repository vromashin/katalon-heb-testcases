<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>minicart-menu-switch</name>
   <tag></tag>
   <elementGuidId>6117d13c-1372-4188-9cf6-566981bb528e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot;block-title icon-chevron-left&quot;]/button[@class=&quot;details side-cart-menu-switch&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
