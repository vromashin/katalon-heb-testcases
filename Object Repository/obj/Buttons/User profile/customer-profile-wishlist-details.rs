<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>customer-profile-wishlist-details</name>
   <tag></tag>
   <elementGuidId>47eccb03-beb3-40ca-9a56-3de006e1c357</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body[@data-container='body']/div[@class='page-wrapper']/main[@id='maincontent']/div[@class='columns']/div[@class='column main']/div[@class='products-grid wishlist']/div[@class='block-wishlist-management']/table[@class='lists-table table customer-wishlists']/tbody/tr[2]/td[4]/a[1][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
