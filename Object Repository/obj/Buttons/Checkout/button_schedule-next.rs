<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_schedule-next</name>
   <tag></tag>
   <elementGuidId>7bee2e3b-8cfd-4c83-ae34-1e763680cc9f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='shipping-method-buttons-container']//div[@class='primary']//button[@data-role='opc-continue']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
