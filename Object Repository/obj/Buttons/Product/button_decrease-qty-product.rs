<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_decrease-qty-product</name>
   <tag></tag>
   <elementGuidId>3d597103-01e8-4c7c-82df-7ca5099bd1ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[contains(@id, 'maincontent')]//a[@class='reduce']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
