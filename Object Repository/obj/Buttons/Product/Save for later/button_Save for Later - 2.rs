<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Save for Later - 2</name>
   <tag></tag>
   <elementGuidId>82629000-c8c5-4462-9b36-3d2e6170edb8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;maincontent&quot;]/div/div[1]/div[6]/ol/li[4]/div[2]/div/div[1]/div[3]/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//button[@type = 'button' and (text() = '
            Save for Later
        ' or . = '
            Save for Later
        ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-product-id</name>
      <type>Main</type>
      <value>316089</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-params</name>
      <type>Main</type>
      <value>{&quot;action&quot;:&quot;https://qa.cert.hebtoyou.net/wishlist/index/add/&quot;,&quot;data&quot;:{&quot;product&quot;:&quot;316089&quot;,&quot;uenc&quot;:&quot;aHR0cHM6Ly9xYS5jZXJ0LmhlYnRveW91Lm5ldC9jYXRhbG9nL2NhdGVnb3J5L3ZpZXcvcy92ZWdldGFibGVzL2lkLzE5ODQv&quot;}}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>label action split wishlist-dropdown</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Save for Later
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;products wrapper grid products-grid&quot;]/ol[@class=&quot;products list items product-items&quot;]/li[@class=&quot;item product product-item&quot;]/div[@class=&quot;product-item-info&quot;]/div[@class=&quot;product details product-item-details&quot;]/div[@class=&quot;product-actions clear&quot;]/div[@class=&quot;actions-secondary&quot;]/div[@class=&quot;split button wishlist&quot;]/button[@class=&quot;label action split wishlist-dropdown&quot;]</value>
   </webElementProperties>
</WebElementEntity>
