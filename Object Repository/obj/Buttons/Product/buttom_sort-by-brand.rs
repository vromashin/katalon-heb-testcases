<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttom_sort-by-brand</name>
   <tag></tag>
   <elementGuidId>87d5d15e-fe41-418a-a6fe-64a932225475</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/div[@class='brands']/div[contains(@class, 'brand-filter')]/div[contains(@class, 'brand-filter')]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
