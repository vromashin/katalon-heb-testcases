<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_atc-clp-weighted</name>
   <tag></tag>
   <elementGuidId>2b39f35d-8df6-4e1d-a20c-fc1c33f7b6e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[contains(@id, 'maincontent')]//div[contains(@class, 'product')]//div[@class='select-weight']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
