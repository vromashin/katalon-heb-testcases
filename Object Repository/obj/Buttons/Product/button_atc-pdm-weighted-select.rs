<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_atc-pdm-weighted-select</name>
   <tag></tag>
   <elementGuidId>29b168b5-7fb1-4ac6-b02d-eacfaac50b43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@id, 'modal-content')]//div[contains(@class, 'product')]//div[@class='select-weight']/select/option[last()]

</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
