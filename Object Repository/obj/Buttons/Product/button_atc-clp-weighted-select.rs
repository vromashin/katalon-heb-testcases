<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_atc-clp-weighted-select</name>
   <tag></tag>
   <elementGuidId>3409a211-b12b-4126-b1af-dde156f32ee7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[contains(@id, 'maincontent')]//div[contains(@class, 'product')]//div[@class='select-weight']/select/option[last()]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
