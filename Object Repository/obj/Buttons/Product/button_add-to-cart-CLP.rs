<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_add-to-cart-CLP</name>
   <tag></tag>
   <elementGuidId>b26b4f32-585b-415b-bb4d-438e15f84de7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='page-wrapper']//div[1]//ol/li[1]/div[contains(@class, &quot;product&quot;)]/div[contains(@class, &quot;product&quot;)]//div[contains(@class, 'actions-primary')]/form/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
