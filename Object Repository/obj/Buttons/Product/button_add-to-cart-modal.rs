<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_add-to-cart-modal</name>
   <tag></tag>
   <elementGuidId>281b673a-5b3a-4949-8b68-54327563c54a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='add-to-wrapper']//div[@class='actions-primary']//form[@data-role='tocart-form']//div[@class='button-wrapper']//button[@type='submit']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
