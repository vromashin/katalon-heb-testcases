<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_add-to-cart-PDM-suggest</name>
   <tag></tag>
   <elementGuidId>1c69f6f3-b12c-42fa-b4c0-39d1fe9a9247</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class, 'upsell')]/ol/li[1]//div[contains(@class, 'actions')]//button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
