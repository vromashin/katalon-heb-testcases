<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_reset-filter-by-brand</name>
   <tag></tag>
   <elementGuidId>f0fbb497-42cb-4e29-8bdc-3afadc30c103</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/div[@class='brands']/div[contains(@class, 'brand-filter')]/div[contains(@class, 'brand-filter')]/div/div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
