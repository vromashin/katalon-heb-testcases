<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Breadcrumbs-homepage</name>
   <tag></tag>
   <elementGuidId>3e995e38-9751-4492-b958-583138fe8c3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='breadcrumbs']/ul/li[last()]/preceding-sibling::li[last()]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
