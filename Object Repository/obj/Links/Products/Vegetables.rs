<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Vegetables</name>
   <tag></tag>
   <elementGuidId>ef5ca7e4-c7fb-4348-b73f-a5fa7000665e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[contains(@class, 'categories')]/li[1]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = 'https://qa.cert.hebtoyou.net/catalog/category/view/s/vegetables/id/1984/' and (text() = '
                Vegetables
            ' or . = '
                Vegetables
            ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>image show</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/catalog/category/view/s/vegetables/id/1984/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Vegetables
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/ul[@class=&quot;subcategories categories-grid&quot;]/li[@class=&quot;subcategory category&quot;]/a[@class=&quot;image show&quot;]</value>
   </webElementProperties>
</WebElementEntity>
