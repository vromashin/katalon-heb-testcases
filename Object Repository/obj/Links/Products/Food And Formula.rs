<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Food And Formula</name>
   <tag></tag>
   <elementGuidId>ce645759-a151-46fb-b8f6-fa986cb7420a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/div[2]/main[1]/div[1]/div[1]/ul[1]/li[5]/a[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//body[@data-container='body']/div[@class='page-wrapper']/main[@id='maincontent']/div[@class='columns']/div[@class='column main']/ul[@class='subcategories categories-grid']/li[5]/a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//body[@data-container='body']/div[@class='page-wrapper']/main[@id='maincontent']/div[@class='columns']/div[@class='column main']/ul[@class='subcategories categories-grid']/li[5]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
