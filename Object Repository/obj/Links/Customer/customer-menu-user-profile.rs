<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>customer-menu-user-profile</name>
   <tag></tag>
   <elementGuidId>708c14d1-94f9-489f-b5ab-78d39d3e687b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value> //a[@href='https://qa.cert.hebtoyou.net/customer/account/']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
