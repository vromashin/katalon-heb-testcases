<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Logout</name>
   <tag></tag>
   <elementGuidId>e65031ff-8579-40cb-9488-cfdac9e943f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[2]/header/div[3]/div/ul[1]/li/div/ul/li[4]/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = 'https://qa.cert.hebtoyou.net/customer/account/logout/' and (text() = '
        Logout    ' or . = '
        Logout    ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/customer/account/logout/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-post</name>
      <type>Main</type>
      <value>{&quot;action&quot;:&quot;https:\/\/qa.cert.hebtoyou.net\/customer\/account\/logout\/&quot;,&quot;data&quot;:{&quot;uenc&quot;:&quot;aHR0cHM6Ly9xYS5jZXJ0LmhlYnRveW91Lm5ldC8,&quot;}}</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Logout    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column minicart-active&quot;]/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[@class=&quot;customer-welcome active&quot;]/div[@class=&quot;customer-menu&quot;]/ul[@class=&quot;customer links&quot;]/li[@class=&quot;authorization-link&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
