<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>customer-menu-user-wishlist</name>
   <tag></tag>
   <elementGuidId>a99fc6c4-92ea-4d4a-9c19-158d16730cd1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@href='https://qa.cert.hebtoyou.net/wishlist/'][contains(text(),'Save for Later Lists')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
