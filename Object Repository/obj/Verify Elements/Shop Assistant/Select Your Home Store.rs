<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Select Your Home Store</name>
   <tag></tag>
   <elementGuidId>5745a8a3-9337-4732-81c0-8a2604aa2b63</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[contains(text(),'Select your Home Store')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
