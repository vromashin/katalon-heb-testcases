<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ATC Weighted Product</name>
   <tag></tag>
   <elementGuidId>5b97da0c-efc2-472a-9162-34bb6987204b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[contains(@id, 'maincontent')]//div[contains(@class, 'product')]//div[@class='select-weight']//button[contains(@class, 'added')]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
