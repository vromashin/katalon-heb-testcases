<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ATC Weighted Product</name>
   <tag></tag>
   <elementGuidId>65fd2f4d-40d7-4d2a-86a5-54a070887073</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@id, 'modal-content')]//div[contains(@class, 'product')]//div[@class='select-weight']//button[contains(@class, 'added')]/span
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
