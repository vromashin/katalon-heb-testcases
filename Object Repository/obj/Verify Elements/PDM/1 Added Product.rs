<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>1 Added Product</name>
   <tag></tag>
   <elementGuidId>6e72c11e-9334-4a74-b355-9cd123a0f3b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='modal']//div[contains(@class, 'product-info-wrapper')]//div[contains(@class, 'actions')]//button[@type='submit']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
