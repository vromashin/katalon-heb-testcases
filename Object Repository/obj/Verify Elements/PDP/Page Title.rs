<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Page Title</name>
   <tag></tag>
   <elementGuidId>18decd04-ee10-466a-bb8c-2eb468569ed1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h1[@class='page-title']/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
