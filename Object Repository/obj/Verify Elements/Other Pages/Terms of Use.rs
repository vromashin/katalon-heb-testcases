<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Terms of Use</name>
   <tag></tag>
   <elementGuidId>2b4dc763-4535-474c-a7d2-66eda2093869</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h2/strong[contains(text(), &quot;Terms of Use&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
