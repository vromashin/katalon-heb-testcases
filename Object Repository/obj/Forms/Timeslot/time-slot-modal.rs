<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>time-slot-modal</name>
   <tag></tag>
   <elementGuidId>0f7c67b9-5932-49f1-8f9c-9d102c360f05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='parent-shopping-selector-wrapper custom-context-modal no-background zoom-in-open']//div[@id='timeslot-view-wrapper']//div[@class='store-selector']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
