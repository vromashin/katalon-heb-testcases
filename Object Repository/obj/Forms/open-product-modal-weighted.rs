<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>open-product-modal-weighted</name>
   <tag></tag>
   <elementGuidId>f60e96ed-c126-4a67-8c7e-7229c3bbc782</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//main[contains(@id, 'maincontent')]//div[contains(@class, 'product')]//div[@class='select-weight']/ancestor::div[@class='product-item-info']/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
