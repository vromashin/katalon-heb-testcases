<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>gigya-login-form</name>
   <tag></tag>
   <elementGuidId>bfa14903-29c5-4349-9d43-d1b083b745d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id = &quot;gigya-login-form&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-width</name>
      <type>Main</type>
      <value>auto</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>gigya-conditional:class</name>
      <type>Main</type>
      <value>viewport.width &lt; 550 ?gigya-screen v2 portrait mobile:viewport.width &lt; 920 ?gigya-screen v2 portrait:</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>gigya-screen v2 landscape</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>gigya-expression:data-caption</name>
      <type>Main</type>
      <value>screenset.translations['GIGYA_LOGIN_SCREEN_CAPTION']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-screenset-element-id</name>
      <type>Main</type>
      <value>gigya-login-screen</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-screenset-element-id-publish</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-screenset-roles</name>
      <type>Main</type>
      <value>instance</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>gigya-login-screen</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-caption</name>
      <type>Main</type>
      <value>Login</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
            
            
            
                
                    
                    
                
                
                
                
                    
                        
                        
                    
                    
                        
                        
                    
                    
                        
                    
                        
                        
                        
                        
                    
                    
                        
                    
                Forgot your password?
                
                
            
            
                
                
            
            
            
        
    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gigya-login-screen&quot;)</value>
   </webElementProperties>
</WebElementEntity>
