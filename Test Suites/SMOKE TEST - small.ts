<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SMOKE TEST - small</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-06-22T18:23:45</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>1f4492bc-da54-4315-b52a-ef6d3cc0a11d</testSuiteGuid>
   <testCaseLink>
      <guid>f5d90962-d489-47f6-bfda-d47935efa7db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Check that custoer can log out</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97b7c517-0d8c-44e0-8763-ea71d948b075</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Check that customer can create account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f8a75bd-d6a1-46d8-b980-7d4798c9c74b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Check that product can be found via search field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76ae52ba-4f15-4f99-b07f-5d60c11223c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Check that store can be selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34cdb904-fa65-4cbc-bae2-8e4f1115b8c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Review product in the cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99fe8b92-3f87-4640-bac3-aa3dc204bc0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Review that user can remove item from the cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b41f13fb-600a-4714-813b-0a4deaef3c9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verifiy that home page has no layout issues</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06317a7d-76b1-4481-8dfe-c6ddcfa3adcc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that Catalog Listing Page can be opened</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bad73bd6-ea89-4ce7-9563-059c02299d1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that customer can change password and log in with new one</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4aa1ec3f-4314-4486-ade9-9c9b8680496d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that customer can edit and add account information</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1864d2ff-68d4-469c-8b16-c208a1648259</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that customer can log in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96dd0d1e-7939-405e-a9de-b5a74d4a6485</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that minicart can be emptied</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64c9cb99-1474-4977-befa-a8fce7f8287e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that order can be placed using HPG payment method</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16824d02-d1e8-4c3c-8cc1-bcaa7ff33556</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that product can be added to the cart from Catalog Listing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da4f9ad5-54c0-43d2-b984-47f322c841f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that product can be added to the cart from the Product Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97023d59-184d-41ec-91bd-dfac29b4c33f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Details Modal/Verify that Product Modal can be opened</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c25cef9f-2e7e-4bb7-b973-3d62798d0349</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that time and date can be selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b7d5d57-eaf3-41c2-9091-7c2043a2a64d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that user can access to the -Save for Later- lists</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec75431d-90f3-4f56-88a5-e03bd0a00777</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SMOKE TEST/Verify that user can open mini-cart on the right side</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
