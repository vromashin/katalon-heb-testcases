<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Catalog Details Page</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-05T11:15:54</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>5d38eaff-29b1-48c6-badc-a0cab7b9fcfd</testSuiteGuid>
   <testCaseLink>
      <guid>2b3ad379-f7bb-4e29-a87e-f6d90c248580</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that -Sorting By Brand- filters can be applied and reseted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c550aa1d-98b9-42f5-9218-b8716ba70ba1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that Category details Page L1 can be opened</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f64ac76e-3b0b-4f5c-b20e-7ebef8b099ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that Category details Page L3 can be opened</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c78dd51-46b2-4f5d-927d-d1f6ba624c84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that filtering is applied correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3255a7be-8165-4d57-af7e-00aac153b579</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that product can be added to the -Save for Later- lists</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>649bbaa2-0a05-4075-9cbd-d2711efa4f99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that product can be added to the cart from Catalog Listing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b98cc4a-95e4-402e-867c-6fa5068092f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that product quantity can be decreased in the cart via Decrement icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77c3f957-e8a4-4054-b675-7d36d6eb3e8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that product quantity can be increased in the cart via Increment on the Add To Cart button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7815312-6045-4f61-8393-38e45da67be9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that Sorting is applied correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f4f092e-e9b3-416a-a970-f6772dce1b93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that user can go back the the categories L2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af13c507-04d1-433c-9eea-359fa68a3c59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that customer can decline Cross Sell product to add it to the cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a056446-26ca-4ad7-8196-0147211a5f8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify that Sorting and Filtering can be applied at once</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b72e1f3-d900-4469-8163-d289786e5607</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Catalog Details Page/Verify breadcrumbs functionality on the category page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
