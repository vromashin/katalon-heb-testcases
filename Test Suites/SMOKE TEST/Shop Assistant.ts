<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Shop Assistant</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-05T11:28:38</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>a1e9a366-ef38-4ad3-8feb-f8708006f476</testSuiteGuid>
   <testCaseLink>
      <guid>dc783d9e-5511-411d-9d7b-f78889bcfd25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Shop Assistant/Verify that selected time in time slot is reset if delivery method is changed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f94b506-9b15-4f26-b46e-2f1b06978b14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Shop Assistant/Verify that selected time in time slot is reset if store location from Curbside Pickup is changed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2816e32-251c-410b-b8ff-f38ffff24b1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Shop Assistant/Verify that stores list is available if delivery type is Curbside Pickup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2ad56f0-a3bf-4943-908a-018e40d60931</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Shop Assistant/Verify that the Last Available Day in Time Slot can be selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4dc26d3a-76d7-4bca-95a0-9cc76e986613</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Shop Assistant/Verify that time and date can be selected</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
