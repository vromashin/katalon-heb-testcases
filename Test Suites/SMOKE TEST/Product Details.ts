<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Product Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-05T10:52:54</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>2ccf8bf2-4acb-4a09-9ce6-bb96ad470a9f</testSuiteGuid>
   <testCaseLink>
      <guid>c62ca1eb-7a95-477a-880c-4fa4bb6e1a1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Details Modal/Verify that Product Modal can be opened</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56b140c9-929f-4924-8ad3-c3482a5ec47e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Details Modal/Verify that product can be added to the cart from the Product Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43bab64b-ed03-4553-90af-9f093de370d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Details Modal/Verify that tabs in product modal is available and can be viewed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4bd65a14-5485-481a-a91f-4d5ebe84b766</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Details Modal/Verify that Application is allowing customer to open PDP page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>529c5aa1-2c52-4fee-abfa-65b2dda2b48c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Details Modal/Verify that Product from -May We Suggest- can be added to the cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58e9496b-7ff2-4e53-80a6-40dbca9de503</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Check Save For Later button functionality while being signed in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14051684-561b-462d-8ed0-aaab747762b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Test breadcrumbs functionality on the Product Details Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd69b832-5506-4cba-a849-81c6287ef45a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Verify that all associated products can be deleted from the cart via - option on Add To Cart button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>346784a3-7d1a-4b5b-af42-65879062a8cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Verify that associated product quantity in the cart can be decreased via clicking Add To Cart Button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c1ed6d3-7532-4599-9839-5ed28107fedd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Verify that associated product quantity in the cart can be increased via clicking Add To Cart Button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d7a2768-6e11-4a1a-9e34-c0bc4710b90b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Verify that Related products can be added to the cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7285fe39-d4c7-48ea-9df3-7348bd8a25c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Verify that tabs is available and can be viewed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85334463-6e33-45a9-8fbd-ee9ca47bc2af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Verufy that customer can add product to the cart while being signed in</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3610ff1-14e6-4aca-a903-23c77d0fc9d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Dedails Page/Verufy that customer can add product to the cart while being signed out</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d52b5f9-94ae-41e5-bd79-8120b2133090</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Product Details Modal/Verify that related products can be aded to the cart from QuickView</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
