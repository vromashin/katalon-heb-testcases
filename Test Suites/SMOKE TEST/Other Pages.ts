<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Other Pages</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-05T11:24:18</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>1</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <testSuiteGuid>a2850160-8583-43e9-afc2-f77f9f674bec</testSuiteGuid>
   <testCaseLink>
      <guid>578f49d0-6dd5-4435-b44a-552576923186</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Other Pages/Review Contact Us page layout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1564ca8d-ee16-44e7-91fa-3865d06f4511</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Other Pages/Review FAQs page layout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e11108b-a69f-41db-ac39-5aacdc91f400</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Other Pages/Review Privacy and Policy page layout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a9d9131-092f-4031-9b95-b7baeb3f1e75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/MAIN SMOKE TEST/Other Pages/Review Terms and Condition layout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
