import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String chars = "abcdefghijklmnopqrstuvwxyz0123456789"

//random email address
GlobalVariable.randomEmail = (randomString(chars, 10) + '@example.com')
randomName = randomString(chars, 10)

//return randomEmail

WebUI.click(findTestObject('obj/Links/Customer/Register'))

WebUI.setText(findTestObject('obj/Text Fields/input_email-register'), GlobalVariable.randomEmail)

WebUI.focus(findTestObject('obj/Text Fields/input_password-register'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('obj/Text Fields/input_password-register'), GlobalVariable.default_password)

WebUI.setText(findTestObject('obj/Text Fields/input_passwordRetype-register'), GlobalVariable.default_password)

WebUI.setText(findTestObject('obj/Text Fields/input_profile.firstName-register'), randomName)

WebUI.setText(findTestObject('obj/Text Fields/input_profile.lastName-register'), randomName)

WebUI.click(findTestObject('obj/Buttons/Register/input_gigya-input-submit-register'))

static String randomString(String chars, int length) {
    Random rand = new Random()

    StringBuilder sb = new StringBuilder()

    for (int i = 0; i < length; i++) {
        sb.append(chars.charAt(rand.nextInt(chars.length())))
    }
    
    return sb.toString()
}

