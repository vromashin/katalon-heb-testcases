import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('obj/Links/Customer/customer-profile-change-password'))

WebUI.delay(2)

WebUI.focus(findTestObject('obj/Text Fields/Password Change/current-password'))

WebUI.setText(findTestObject('obj/Text Fields/Password Change/current-password'), GlobalVariable.default_password)

WebUI.setText(findTestObject('obj/Text Fields/Password Change/new-password'), GlobalVariable.new_password)

WebUI.setText(findTestObject('obj/Text Fields/Password Change/password-confirm'), GlobalVariable.new_password)

WebUI.click(findTestObject('obj/Buttons/User profile/customer-profile-change-password'))

WebUI.callTestCase(findTestCase('GIVEN/User/Open Customer Menu'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('obj/Links/Customer/Logout'))

WebUI.click(findTestObject('obj/Links/Nav/Shop-Select'))

WebUI.waitForElementPresent(findTestObject('obj/Links/Customer/LogIn'), 20)

WebUI.click(findTestObject('obj/Links/Customer/LogIn'))

WebUI.waitForElementPresent(findTestObject('obj/Forms/gigya-login-form'), 20)

WebUI.focus(findTestObject('obj/Forms/gigya-login-form'))

WebUI.click(findTestObject('obj/Text Fields/input_username-login'))

WebUI.setText(findTestObject('obj/Text Fields/input_username-login'), GlobalVariable.randomEmail)

WebUI.click(findTestObject('obj/Text Fields/input_password-login'))

WebUI.setText(findTestObject('obj/Text Fields/input_password-login'), GlobalVariable.new_password)

WebUI.click(findTestObject('obj/Buttons/Register/input_gigya-input-submit'))

